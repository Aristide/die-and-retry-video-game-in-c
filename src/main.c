#include <stdlib.h>
#include <stdio.h>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"

#include "jeu.h"

int choix(SDL_Surface *ecran, SDL_Renderer *renderer);
void Flip(SDL_Surface *ecran, SDL_Renderer *renderer);

int main ()
{
    SDL_Init(SDL_INIT_VIDEO);
    int done = 0, tir = 0, gagne = 2, niveau = 1;
    SDL_Event event;
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024);
    Mix_Chunk *son = Mix_LoadWAV("src/son/0494.wav"), *menu =Mix_LoadWAV("src/son/menu2.mp3");
    TTF_Font *police = NULL;
    SDL_Color couleur = {255, 255, 255,0};
    SDL_Window *window = SDL_CreateWindow("jeu", 200, 200, 800, 800, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_Surface *ecran = NULL, *accueil = IMG_Load("src/image/santa.jpg"), *jouer = NULL, *texte = NULL;
    SDL_Rect position, positionTexte, positionJouer;
    position.x = 0;
    position.y = 0;
    TTF_Init();

    Mix_PlayChannelTimed(2, menu, 2, 11000);
    police = TTF_OpenFont("src/police/wood.ttf", 65);
    texte = TTF_RenderText_Blended(police, "Jouer", couleur);
    ecran = SDL_CreateRGBSurface(0, 800, 800, 32, 0, 0, 0, 0);
    jouer = SDL_CreateRGBSurface(0, 300, 100, 32, 0, 0, 0, 0);
    SDL_BlitSurface(accueil, NULL, ecran, &position);

    positionJouer.x = accueil->w/2-jouer->w/2;
    positionJouer.y = accueil->h/4*3-jouer->h/2;
    positionTexte.x = accueil->w/2-texte->w/2;
    positionTexte.y = accueil->h/4*3-texte->h/2;

    Mix_Volume(1, MIX_MAX_VOLUME/10);
    Mix_Volume(3, MIX_MAX_VOLUME/10);

    while(!done)
    {
    SDL_WaitEvent(&event);
        if(event.key.keysym.sym == SDLK_ESCAPE)
          done = 1;
        switch(event.type)
        {
        case SDL_MOUSEMOTION:
            if(event.motion.x > accueil->w/2-jouer->w/2 && event.motion.x < accueil->w/2+jouer->w/2 && event.motion.y > accueil->h/4*3-jouer->h/2 && event.motion.y < accueil->h/4*3+jouer->h/2)
                {
                SDL_FillRect(jouer, NULL, SDL_MapRGB(ecran->format, 165, 61, 76));
                if(!tir)
                Mix_PlayChannel(1, son, 0);
                tir = 1;
                }
            else
                {
                SDL_FillRect(jouer, NULL, SDL_MapRGB(ecran->format, 173, 5, 30));
                tir = 0;
                }
        break;
        case SDL_MOUSEBUTTONUP:
            if(event.motion.x > accueil->w/2-jouer->w/2 && event.motion.x < accueil->w/2+jouer->w/2 && event.motion.y > accueil->h/4*3-jouer->h/2 && event.motion.y < accueil->h/4*3+jouer->h/2)
            {
                niveau = choix(ecran, renderer);
                Mix_HaltChannel(2);
                if(niveau != 11)
                  gagne = jeu(ecran, renderer, niveau);
                if (gagne == 1)
                    accueil = IMG_Load("src/image/gagne.jpg");
                else if(gagne == 0)
                    accueil = IMG_Load("src/image/perdu.jpg");
                else if (gagne > 1)
                    {
                    accueil = IMG_Load("src/image/survie.bmp");
                    char temps[14];
                    sprintf(temps, "%d secondes", gagne/1000);
                    SDL_Surface *survie = TTF_RenderText_Blended(police, temps, couleur);
                    SDL_Rect positionTemps;
                    positionTemps.x = 250;
                    positionTemps.y = 300;
                    SDL_BlitSurface(accueil, NULL, ecran, &position);
                    SDL_BlitSurface(survie, NULL, ecran, &positionTemps);
                    SDL_FreeSurface(survie);
                    }
                else
                    accueil = IMG_Load("src/image/santa.jpg");
                if(gagne < 2)
                   SDL_BlitSurface(accueil, NULL, ecran, &position);
            }
            break;
        }
    SDL_BlitSurface(jouer, NULL, ecran, &positionJouer);
    SDL_BlitSurface(texte, NULL, ecran, &positionTexte);
    Flip(ecran, renderer);
    }

    SDL_FreeSurface(accueil);
    SDL_FreeSurface(jouer);
    SDL_FreeSurface(texte);
    TTF_CloseFont(police);
    Mix_FreeChunk(son);

    Mix_CloseAudio();
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
int choix(SDL_Surface *ecran, SDL_Renderer *renderer)
{
    SDL_Surface *accueil = IMG_Load("src/image/cat.jpg"), *lvl = SDL_CreateRGBSurface(0, 200, 200, 32, 0, 0, 0, 0), *texte = NULL, *survie = SDL_CreateRGBSurface(0, 700, 200, 32, 0, 0, 0, 0);
    SDL_Rect position;
    position.x = 0;
    position.y = 0;
    SDL_FillRect(survie, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
    SDL_BlitSurface(accueil, NULL, ecran, &position);
    SDL_FillRect(lvl, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
    position.x = 50;
    position.y = 50;
    SDL_BlitSurface(lvl, NULL, ecran, &position);
    position.x = 300;
    SDL_BlitSurface(lvl, NULL, ecran, &position);
    position.x = 550;
    SDL_BlitSurface(lvl, NULL, ecran, &position);
    position.x = 50;
    position.y = 550;
    SDL_BlitSurface(survie, NULL, ecran, &position);

    TTF_Font *police = TTF_OpenFont("src/police/wood.ttf", 65);
    SDL_Color couleur = {200, 15, 47, 0};

    texte = TTF_RenderText_Blended(police, "1", couleur);
    position.x = 130;
    position.y = 100;
    SDL_BlitSurface(texte, NULL, ecran, &position);

    texte = TTF_RenderText_Blended(police, "2", couleur);
    position.x = 380;
    SDL_BlitSurface(texte, NULL, ecran, &position);

    texte = TTF_RenderText_Blended(police, "3", couleur);
    position.x = 630;
    SDL_BlitSurface(texte, NULL, ecran, &position);

    texte = TTF_RenderText_Blended(police, "Mode Survie", couleur);
    position.x = 250;
    position.y = 600;
    SDL_BlitSurface(texte, NULL, ecran, &position);

    Flip(ecran, renderer);

    int done = 10;
    SDL_Event event;

    while(done == 10)
    {
    SDL_WaitEvent(&event);
    if(event.type == SDL_MOUSEBUTTONUP)
    {
    if(event.motion.x > 50 && event.motion.x < 250 && event.motion.y > 50 && event.motion.y < 250)
        done = 0;
    if(event.motion.x > 300 && event.motion.x < 500 && event.motion.y > 50 && event.motion.y < 250)
        done = 1;
    if(event.motion.x > 550 && event.motion.x < 750 && event.motion.y > 50 && event.motion.y < 250)
        done = 2;
    if(event.motion.x > 50 && event.motion.x < 750 && event.motion.y > 550 && event.motion.y < 750)
        done = 3;
    }
    if(event.key.keysym.sym == SDLK_ESCAPE)
        done = 11;
    }
    SDL_FreeSurface(accueil);
    SDL_FreeSurface(lvl);
    SDL_FreeSurface(texte);
    SDL_FreeSurface(survie);
    TTF_CloseFont(police);
    
    return done;
}

void Flip(SDL_Surface *ecran, SDL_Renderer *renderer)
{
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, ecran);
  SDL_RenderCopy(renderer, texture, NULL, NULL);
  SDL_RenderPresent(renderer);
  SDL_DestroyTexture(texture);
}

