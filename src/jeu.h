#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"

enum {HAUT, BAS, GAUCHE, DROITE};

int jeu(SDL_Surface* ecran, SDL_Renderer *renderer, int niveau);
void Flip(SDL_Surface *ecran, SDL_Renderer *renderer);
int deplacer(int carte[20][20], SDL_Rect positionPersonnage, int direction);
int deplacerMechant(int carte[20][20], SDL_Rect positionMechant, SDL_Rect positionPersonnage, int *pointeurBip, int hasard);

#endif // JEU_H_INCLUDED
