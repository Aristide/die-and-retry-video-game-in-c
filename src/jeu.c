#include "jeu.h"

int jeu(SDL_Surface *ecran, SDL_Renderer *renderer, int niveau)
{
    int carte[20][20] = {{0}};
    FILE* fichier = NULL;
    Mix_Chunk *level = Mix_LoadWAV("src/son/level2.mp3");
    Mix_PlayChannel(1, level, 0);
    int caractere = 0, i, j, tempsDebut = SDL_GetTicks(), tempsActuel = 0, tempsPrecedent = 0, tempsPrecedent2 = 0, touche = 0, bip = 0, nombreMechant = 0, done = -1;
    int *pointeurBip = &bip;
    SDL_Surface *mur = IMG_Load("src/image/mur.bmp"), *personnageActuel = NULL, *arrive = IMG_Load("src/image/simon.png"), *vide = IMG_Load("src/image/ciel.png"), *mechant = IMG_Load("src/image/cat.png"), *lava = IMG_Load("src/image/bombe.png");
    SDL_Surface *personnage[4] = {NULL};
    personnage[HAUT] = IMG_Load("src/image/up.png");
    personnage[BAS] = IMG_Load("src/image/down.png");
    personnage[GAUCHE] = IMG_Load("src/image/left.png");
    personnage[DROITE] = IMG_Load("src/image/right.png");
    personnageActuel = personnage[BAS];
    SDL_Rect position, positionPersonnage;
    for(i = 0;i < 4; i++)
        SDL_SetColorKey(personnage[i], SDL_TRUE, SDL_MapRGB(personnage[i]->format, 255, 255, 255));
    SDL_SetColorKey(arrive, SDL_TRUE, SDL_MapRGB(arrive->format, 255, 255, 255));
    SDL_SetColorKey(mechant, SDL_TRUE, SDL_MapRGB(mechant->format, 255, 255, 255));

    if((fichier = fopen("src/carte/jeu.txt", "r")) == NULL)
        exit(EXIT_FAILURE);

    fseek(fichier, niveau*402, SEEK_SET);
    for(i = 0; i < 20; i++)
    {
        for(j = 0; j < 20; j++)
        {
            caractere = fgetc(fichier);
            if(caractere == 52)
                nombreMechant++;
        }
    }
    int hasard[nombreMechant];
    SDL_Rect positionMechant[nombreMechant], position2[nombreMechant];
    nombreMechant = 0;

    fseek(fichier, niveau*402, SEEK_SET);
    for(i = 0; i < 20; i++)
    {
        for(j = 0; j < 20; j++)
        {
            caractere = fgetc(fichier);
            carte[i][j] = caractere;
            position.x = j*40;
            position.y = i*40;
            switch(carte[i][j])
            {
            case 48://Vide
                SDL_BlitSurface(vide, NULL, ecran, &position);
                break;
            case 49://Block
                SDL_BlitSurface(mur, NULL, ecran, &position);
                break;
            case 50://Personnage
                SDL_BlitSurface(vide, NULL, ecran, &position);
                SDL_BlitSurface(personnageActuel, NULL, ecran, &position);
                positionPersonnage.x = j*40;
                positionPersonnage.y = i*40;
                break;
            case 51://Arrive
                SDL_BlitSurface(arrive, NULL, ecran, &position);
                break;
            case 52://Mechant
                SDL_BlitSurface(vide, NULL, ecran, &position);
                SDL_BlitSurface(mechant, NULL, ecran, &position);
                positionMechant[nombreMechant].x = j*40;
                positionMechant[nombreMechant].y = i*40;
                nombreMechant++;
                break;
            case 53://Lave
              SDL_BlitSurface(lava, NULL, ecran, &position);
              break;
            }
        }
    }
fclose(fichier);

SDL_Event event;

while(done == -1)
{
position.x = positionPersonnage.x;
position.y = positionPersonnage.y;
for(i = 0; i < nombreMechant; i++)
{
position2[i].x = positionMechant[i].x;
position2[i].y = positionMechant[i].y;
}

tempsActuel = SDL_GetTicks();

SDL_PollEvent(&event);

switch(event.type)
{
case SDL_KEYDOWN:
    if(tempsActuel - tempsPrecedent2 > 150 || touche == 1)
    {
    switch(event.key.keysym.sym)
    {
    case SDLK_RIGHT:
        if(deplacer(carte, positionPersonnage, DROITE) == 1)
	{
            positionPersonnage.x += 40;
            personnageActuel = personnage[DROITE];
	}
        break;
    case SDLK_LEFT:
        if(deplacer(carte, positionPersonnage, GAUCHE) == 1)
	{
            positionPersonnage.x -= 40;
            personnageActuel = personnage[GAUCHE];
	}
        break;
    case SDLK_UP:
        if(deplacer(carte, positionPersonnage, HAUT) == 1)
	{
            positionPersonnage.y -= 40;
            personnageActuel = personnage[HAUT];
	}
        break;
    case SDLK_DOWN:
        if(deplacer(carte, positionPersonnage, BAS) == 1)
	{
            positionPersonnage.y += 40;
            personnageActuel = personnage[BAS];
	}
        break;
    case SDLK_ESCAPE:
      return -1;
      break;
    }
    tempsPrecedent2 = tempsActuel;
    touche = 0;
    }
    break;
case SDL_KEYUP:
    touche = 1;
    break;
}
if(tempsActuel - tempsPrecedent > 1000)
{
    for(i = 0; i < nombreMechant; i++)
    {
switch(deplacerMechant(carte, positionMechant[i], positionPersonnage, pointeurBip, hasard[i]))
{
case HAUT:
    positionMechant[i].y -= 40;
    hasard[i] = 1000;
    break;
case BAS:
    positionMechant[i].y +=40;
    hasard[i] = 1000;
    break;
case GAUCHE:
    positionMechant[i].x -= 40;
    hasard[i] = 1000;
    break;
case DROITE:
    positionMechant[i].x += 40;
    hasard[i] = 1000;
    break;
case 4:
    hasard[i] = 0;
    break;
}
    }
tempsPrecedent = tempsActuel;
}
for(i = 0; i < nombreMechant; i++)
    {
if(positionPersonnage.x == positionMechant[i].x && positionPersonnage.y == positionMechant[i].y)
    done = 0;
    }
if(carte[positionPersonnage.y/40][positionPersonnage.x/40] == 51)
    done = 1;
else if(carte[positionPersonnage.y/40][positionPersonnage.x/40] == 53)
    done = 0;
SDL_BlitSurface(vide, NULL, ecran, &position);
SDL_BlitSurface(personnageActuel, NULL, ecran, &positionPersonnage);
for(i = 0; i < nombreMechant; i++)
    {
SDL_BlitSurface(vide, NULL, ecran, &position2[i]);
SDL_BlitSurface(mechant, NULL, ecran, &positionMechant[i]);
    }
Flip(ecran, renderer);
}

for(i = 0; i < 4; i++)
    SDL_FreeSurface(personnage[i]);
SDL_FreeSurface(mur);
SDL_FreeSurface(arrive);
SDL_FreeSurface(vide);
SDL_FreeSurface(mechant);
SDL_FreeSurface(lava);
Mix_FreeChunk(level);

if(niveau == 3)
    return tempsActuel - tempsDebut;
return done;
}

int deplacer(int carte[20][20], SDL_Rect positionPersonnage, int direction)
{
    switch(direction)
    {
    case DROITE:
        if(carte[positionPersonnage.y/40][positionPersonnage.x/40+1] != 49)
            return 1;
        break;
    case GAUCHE:
        if(carte[positionPersonnage.y/40][positionPersonnage.x/40-1] != 49)
            return 1;
        break;
    case HAUT:
        if(carte[positionPersonnage.y/40-1][positionPersonnage.x/40] != 49)
            return 1;
        break;
    case BAS:
        if(carte[positionPersonnage.y/40+1][positionPersonnage.x/40] != 49)
            return 1;
        break;
    }
    return 0;
}

int deplacerMechant(int carte[20][20], SDL_Rect positionMechant, SDL_Rect positionPersonnage, int *pointeurBip, int hasard)
{
    if(positionMechant.x == positionPersonnage.x && hasard == 1000)
    {
        if(positionMechant.y - positionPersonnage.y > 0)
        {
            while(positionMechant.y != positionPersonnage.y)
            {
                if(carte[positionMechant.y/40][positionMechant.x/40] == 49 || carte[positionMechant.y/40][positionMechant.x/40] == 53)
                return 4;
                positionMechant.y -= 40;
            }
            *pointeurBip = 1;
            return HAUT;
        }
        if(positionMechant.y - positionPersonnage.y < 0)
        {
            while(positionMechant.y != positionPersonnage.y)
            {
                if(carte[positionMechant.y/40][positionMechant.x/40] == 49 || carte[positionMechant.y/40][positionMechant.x/40] == 53)
                return 4;
                positionMechant.y += 40;
            }
            *pointeurBip = 1;
            return BAS;
        }
    }
    else if(positionMechant.y == positionPersonnage.y && hasard == 1000)
    {
        if(positionMechant.x - positionPersonnage.x > 0)
        {
            while(positionMechant.x != positionPersonnage.x)
            {
                if(carte[positionMechant.y/40][positionMechant.x/40] == 49 || carte[positionMechant.y/40][positionMechant.x/40] == 53)
                return 4;
                positionMechant.x -= 40;
            }
            *pointeurBip = 1;
            return GAUCHE;
        }
        if(positionMechant.x - positionPersonnage.x < 0)
        {
            while(positionMechant.x != positionPersonnage.x)
            {
                if(carte[positionMechant.y/40][positionMechant.x/40] == 49 || carte[positionMechant.y/40][positionMechant.x/40] == 53)
                return 4;
                positionMechant.x += 40;
            }
            *pointeurBip = 1;
            return DROITE;
        }
    }
    else
    {
        while(1)
        {
            int aleatoire = rand()%4;
            switch(aleatoire)
            {
            case HAUT:
                if (carte[positionMechant.y/40-1][positionMechant.x/40] != 49 && carte[positionMechant.y/40-1][positionMechant.x/40] != 51 && carte[positionMechant.y/40-1][positionMechant.x/40] != 53)
                return HAUT;
                break;
            case BAS:
                if (carte[positionMechant.y/40+1][positionMechant.x/40] != 49 && carte[positionMechant.y/40+1][positionMechant.x/40] != 51 && carte[positionMechant.y/40+1][positionMechant.x/40] != 53)
                return BAS;
                break;
            case GAUCHE:
                if (carte[positionMechant.y/40][positionMechant.x/40-1] != 49 && carte[positionMechant.y/40][positionMechant.x/40-1] != 51 && carte[positionMechant.y/40][positionMechant.x/40-1] != 53)
                return GAUCHE;
                break;
            case DROITE:
                if (carte[positionMechant.y/40][positionMechant.x/40+1] != 49 && carte[positionMechant.y/40][positionMechant.x/40+1] != 51 && carte[positionMechant.y/40][positionMechant.x/40+1] != 53)
                return DROITE;
                break;
            }
        }
    }
return 0;
}

