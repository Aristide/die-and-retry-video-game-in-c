CC = gcc
CFLAGS = -Wall -Wextra -Werror -pedantic -std=c99
SDLFLAGS= -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer

SRC= src/main.c src/jeu.c

all:
	$(CC) $(CFLAGS) $(SRC) $(SDLFLAGS) -o jeu
